# Readme
## Initial setup

* Make a bitbucket account.
* Install git
* Install anaconda
* project is currently python 2.7
* clone this repository

`git clone https://[username]@bitbucket.org/ihopeidontforgetthisaccname/pydnd.git`

* Add webserver alias to git:

`git remote add webserver matthias@jstimpfle.de:~/pydnd/.git`


## Add content

* make sure you have the latest version of the website:

`git pull origin master`

* edit feats.xml, houserules.xml ... in data folder
* Let page run locally for debugging and check that it works:

`python run.py`

* Stage changes for commit:

`git add -u`

* commit changes

`git commit -m "My commit message, it should describe the changes I made"`

* Push changes to master

`git push origin master`

* Deploy changes:

`git push webserver`


## Other git commands

* git status: See all files that are currently staged for commit.
* git diff: See all local changes not yet staged for commit
* git reset: unstage everything
* git reset --hard: undo everything. Use with care
* git log: See commit history

## Add new categories to the webpage
...
