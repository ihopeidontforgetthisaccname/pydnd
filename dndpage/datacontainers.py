# -*- coding: utf-8 -*-
"""
container classes for storing feats, spells, maneuvers, invocations, psi, soulmelds, ...
"""
import inspect
import os
import settings
from xml.etree import ElementTree as et

from utils import listify

# columns
TABLE_VALUES = dict()
TABLE_VALUES["None"] = [0]*6

def to_signed_string(number):
    return '+' + str(number) if number > 0 else str(number)

TABLE_VALUES["Low"] =     [ to_signed_string(x) for x in range(-2,4)]
TABLE_VALUES["Average"] = [ to_signed_string(x) for x in range(0,6)]
TABLE_VALUES["High"] =    [ to_signed_string(x) for x in range(2,8)]

TABLE_VALUES["Poor"] = TABLE_VALUES["Low"]
TABLE_VALUES["Fair"] = TABLE_VALUES["Average"]
TABLE_VALUES["Good"] = TABLE_VALUES["High"]



TEST = 5

def _add(element, name, text):
    if text is not None:
        assert name is not None
        sub_el = et.Element(name)
        sub_el.text = text
        element.append(sub_el)


def insert_feature(element):
    is_global = element.get('globalfeature', default=None)
    name = element.get('name', default=None)
    if name is None:
        name = "default-name"

    if is_global is not None:
        global_feature = settings.features[element.attrib['globalfeature']]
        return Feature(name=name, description=global_feature.description)

    return Feature(name=name, description=total_tostring(element))

def total_tostring(element):
    """
    :return: String converter method for etree elements, which even converts children to strings.
    """
    #global TEST
    #TEST = TEST -1
    #if TEST == 0:
    #    quit()
    p = et.tostring(element, encoding='unicode', method='xml')

    # to cut the tag off
    p = p[p.find('>')+1:]
    p = p[0:p.rfind('<')]
    return p


def init_container(kw):
    self = kw["self"]
    #kw = deepcopy(kw)
    del kw["self"]
    Container.__init__(self, **kw)


class Container(object):
    """
    Abstract container for dnd data e.g. feats, maneuvers...
    """

    def __init__(self, name, **kwargs):
        assert '_keys' not in kwargs.keys()
        self.name = name
        self.ref = remove_spaces(name)
        self._keys = [key for key in kwargs.keys()]
        self._keys.sort()

        # self._keys.append(self.name)

        for key, value in kwargs.items():
            if value is None:
                pass
            # elif hasattr(value, '__iter__'):
            #     value = [x for x in value]
            else:
                value = str(value)

            setattr(self, key, value)

    def get_xml(self, el_name=None):

        if el_name is None:
            classname = self.__class__.__name__
            if classname is not None:
                el_name = classname
            else:
                el_name = 'noname'

        el = et.Element(el_name, name=self.name)

        for key in self._keys:
            _add(el, key, getattr(self, key))

        return el

    def print_xml(self, el_name=None):

        xml = self.get_xml(el_name=el_name)
        print(et.tostring(xml))

class Feature(Container):
    def __init__(self, name, description):
        init_container(locals())

class Item(Container):
    def __init__(self, name, kind, flavor, description, short, slot=None, legacy=None):
        init_container(locals())


class Castable(Container):
    """
    Container for spells, invocations, maneuvers, psi abilities, utterances
    """

    def __init__(self, name, description, school=None, level=None, flavor=None, cast_action=None,
                 target=None, duration=None, save=None, area=None, range=None, descriptors=None):
        """
        >>> x = Castable(name='goo cast', level=9, description='this cast is goo', flavor='you are goo', target='some poor guy')
        >>> x.print_xml()
        b'<Castable name="goo cast"><level>9</level><description>this cast is goo</description><flavor>you are goo</flavor><target>some poor guy</target></Castable>'

        """
        init_container(locals())


class Incantation(Castable):
    """
    Container for incantations
    """

    def __init__(self, name, description, school=None, level=None, flavor=None, cast_action=None,
                 target=None, duration=None, save=None, area=None, range=None, focus=None, material=None,
                 failure=None, extra_casters=None, backlash=None,
                 skill_checks=None):
        init_container(locals())


class Spell(Castable):
    """
    Container for spells
    """

    def __init__(self, name, description, school=None, level=None, flavor=None, cast_action=None,
                 target=None, duration=None, save=None, area=None, range=None, focus=None, material=None,
                 failure=None, extra_casters=None, backlash=None, skill_checks=None, descriptors=None):
        init_container(locals())


class Utterance(Castable):
    """
    Container for Utterances
    """

    def __init__(self, name, description, school=None, level=None, flavor=None, cast_action=None,
                 target=None, duration=None, save=None, area=None, range=None, focus=None, material=None,
                 failure=None, extra_casters=None, backlash=None, skill_checks=None, descriptors=None):
        init_container(locals())


class Psipower(Castable):
    """
    Container for Psipowers
    """

    def __init__(self, name, description, school=None, level=None, flavor=None, cast_action=None,
                 target=None, duration=None, save=None, area=None, range=None, focus=None, material=None,
                 failure=None, extra_casters=None, backlash=None, skill_checks=None, descriptors=None):
        init_container(locals())


class Invocation(Castable):
    """
    Container for Invocations
    """

    def __init__(self, name, description, school=None, level=None, flavor=None, cast_action=None,
                 target=None, duration=None, save=None, area=None, range=None, focus=None,
                 material=None,
                 failure=None, extra_casters=None, backlash=None, skill_checks=None, descriptors=None):
        init_container(locals())


class Maneuver(Castable):
    def __init__(self, name, level, description, school=None, flavor=None, cast_action='standard',
                 prerequisites=None,
                 target=None, duration=None, save=None, area=None, range=None, type=None,
                 descriptors=None):
        """
        >>> x = Maneuver(name='goo strike', level=9, description='this strike is goo', flavor='you are goo', target='some poor guy')
        >>> xml = x.get_xml()
        >>> et.tostring(xml)
        b'<Maneuver name="goo strike"><cast_action>standard</cast_action><level>9</level><description>this strike is goo</description><flavor>you are goo</flavor><prerequisites>[]</prerequisites><target>some poor guy</target></Maneuver>'
        """
        init_container(locals())


class Houserules(Container):
    """
    Container house rules
    """

    def __init__(self, name, description):
        init_container(locals())


class Archetype(Container):
    def __init__(self, name, classes, lesser_archetype_power,
                 moderate_archetype_power, greater_archetype_power,
                 free_text=""):
        self.name = name
        self.ref = remove_spaces(name)
        self.lesser_archetype_power = lesser_archetype_power
        self.moderate_archetype_power = moderate_archetype_power
        self.greater_archetype_power = greater_archetype_power
        self.free_text = free_text
        self.classes = classes

    def connect_to_classes(self, class_dict):
        for c in self.classes:
            if remove_spaces(c) in class_dict:
                class_dict[c].archetypes.append(self)


class Subrace(Container):
    def __init__(self, name, attributes, features, lesser_racial_power=None,
                 moderate_racial_power=None, greater_racial_power=None):

        init_container(locals())
        self.features = features


class Race(Container):
    def __init__(self, name, favored_skills, attributes, racial_type, size, subraces, movement,
                 features, lesser_racial_power, moderate_racial_power, greater_racial_power):
        init_container(locals())

        # container does not work for dicts or lists
        self.favored_skills = ','.join(favored_skills)
        self.features = features
        self.subraces = subraces


class Class(Container):
    @property
    def skills(self):
        return ", ".join(self._skills)

    # possible saves are "Good","Poor", base attack bonuses are
    # "Average","High","Low"
    def __init__(self, name, fortitude="Poor", reflex="Poor", will="Poor",
                 base_attack_bonus="Low", skill_points="2", hit_point_rate="d4",
                 skills=["Craft(any)", "Profession(any)"],
                 class_features=[[(1, "dasd")]], proficiencies="""The character
                 is not proficient with any armor nor shields, but with simple
                 weapons."""):
        self.name = name
        self.ref = remove_spaces(name)
        self.fortitude = fortitude
        self.reflex = reflex
        self.will = will
        self.base_attack_bonus = base_attack_bonus
        self.skill_points = skill_points
        self._skills = skills
        self.proficiencies = proficiencies
        self.hit_point_rate = hit_point_rate
        self.class_features = class_features
        levels = range(1, 7)
        table2 = []
        table2.append(["Level", "BAB", "Fort", "Will",
                       "Ref", "Class Features"])
        for i in range(len(levels)):
            row = []
            row.append(levels[i])
            row.append(TABLE_VALUES[base_attack_bonus][i])
            row.append(TABLE_VALUES[fortitude][i])
            row.append(TABLE_VALUES[will][i])
            row.append(TABLE_VALUES[reflex][i])

            feature_names_by_level = [x[0] for x in class_features[i]]
            if i == 0:
                feature_names_by_level.append("Lesser Archetype Power")
            elif i == 1:
                feature_names_by_level.append("Lesser Racial Power")
            elif i == 2:
                feature_names_by_level.append("Moderate Archetype Power")
            elif i == 3:
                feature_names_by_level.append("Moderate Racial Power")
            elif i == 5:
                feature_names_by_level.append("Greater Archetype Power")
                feature_names_by_level.append("Greater Racial Power")

            row.append(", ".join(feature_names_by_level))
            table2.append(row)

        self.table = list(zip(*table2))
        self.archetypes = []


class Feat(Container):
    @property
    def prerequisites(self):
        return ", ".join(self._prerequisites)

    def __init__(self, name, benefit, classes, archetypes, races, short, flavor='missing', prerequisites=[],
                 descriptors="General", specials=[], legacy=None):

        self.name = name
        self.ref = remove_spaces(name)
        self.flavor = flavor
        self.benefit = benefit
        self.classes = classes
        self.archetypes = archetypes
        self.races = races
        self.short = short
        self._prerequisites = listify(prerequisites)
        self.descriptors = listify(descriptors)
        self.specials = listify(specials)
        self.legacy = legacy

    def get_xml(self):
        """
        Creates xml Element representing the feat
        >>> x = Feat(name='cool name', benefit='no benefit')
        >>> et.tostring(x.get_xml())
        b'<feat name="cool name"><descriptor>General</descriptor><flavor>missing</flavor><benefit>no benefit</benefit></feat>'

        """
        feat = et.Element('feat', name=self.name)

        for desc_txt in self.descriptors:
            desc = et.Element('descriptor')
            desc.text = desc_txt
            feat.append(desc)

        # flavor
        flavor = et.Element('flavor')
        flavor.text = self.flavor
        feat.append(flavor)

        # prerequisites
        for prereq_txt in self._prerequisites:
            prereq = et.Element('prerequisite')
            prereq.text = prereq_txt
            feat.append(prereq)

        # benefit
        benefit = et.Element('benefit')
        benefit.text = self.benefit

        feat.append(benefit)

        # special
        for special_txt in self.specials:
            spec = et.Element('special')
            spec.text = special_txt
            feat.append(spec)

        return feat


class Character(Container):

    def __init__(self, name, str, dex, con, int, wis, cha, experience, race, classes, short, description, status):
        init_container(locals())


def xml2maneuver(el):
    """
    Returns Maneuver from xml element encoding maneuver

    :type el: Element
    :rtype: Maneuver

    >>> x = Maneuver(name='goo strike', level=9, description='this strike is goo', flavor='you are goo', target='some poor guy')
    >>> xml = x.get_xml()
    >>> et.tostring(xml)
    b'<Maneuver name="goo strike"><cast_action>standard</cast_action><level>9</level><description>this strike is goo</description><flavor>you are goo</flavor><prerequisites>[]</prerequisites><target>some poor guy</target></Maneuver>'

    >>> y = xml2maneuver(xml)
    >>> type(y)
    <class '__main__.Maneuver'>
    >>> yml = y.get_xml()
    >>> et.tostring(yml)
    b'<Maneuver name="goo strike"><cast_action>standard</cast_action><level>9</level><description>this strike is goo</description><flavor>you are goo</flavor><prerequisites>[]</prerequisites><target>some poor guy</target></Maneuver>'

    """

    return xml2container(el, container_type=Maneuver)


def xml2archetype(node):
    """

    :param node:
    :return:
    """
    name = node.attrib['name'].title()
    free_text = ''
    if node.findall('free_text'):
        free_text = total_tostring(node.findall('free_text')[0])
    lap = total_tostring(node.findall('lesser_archetype_power')[0])
    marp = total_tostring(node.findall('moderate_archetype_power')[0])
    gap = total_tostring(node.findall('greater_archetype_power')[0])
    classes = [c.text for c in node.findall('class')]
    return Archetype(name=name, lesser_archetype_power=lap,
                     moderate_archetype_power=marp, greater_archetype_power=gap,
                     free_text=free_text, classes=classes)


def xml2class(node):
    """

    :param node:
    :return:
    """
    name = node.attrib['name'].title()
    will = node.attrib['will']
    reflex = node.attrib['ref']
    fortitude = node.attrib['fort']
    hit_point_rate = node.attrib['hpr']
    base_attack_bonus = node.attrib['bab']
    skill_points = node.attrib['skillpoints']
    skills = node.findall('skills')[0].text.split(',')
    proficiencies = node.findall('proficiencies')[0].text
    class_features = [[]] * 6
    for feature_node in node.findall('feature'):
        level = int(feature_node.attrib['level']) - 1  # the array is indexed by the array and starts at 0
        actual_feature = insert_feature(feature_node)
        if len(class_features[level]) == 0:
            class_features[level] = [[actual_feature.name, actual_feature.description]]
        else:
            class_features[int(feature_node.attrib['level']) -
                           1].append([actual_feature.name, actual_feature.description])

    return Class(name=name, fortitude=fortitude, reflex=reflex,
                 will=will, base_attack_bonus=base_attack_bonus,
                 skill_points=skill_points, hit_point_rate=hit_point_rate, skills=skills,
                 class_features=class_features, proficiencies=proficiencies)


def xml2race(node):
    # node attributes
    name = node.attrib['name'].title()
    size = node.attrib['size'].title()
    movement = node.attrib['movement']
    racial_type = node.attrib['racial_type'].title()
    attributes = node.attrib['attributes']

    favored_skills = node.findall('favored_skills')[0].text.split(',')

    # powers
    lrp = None
    mrp = None
    grp = None
    for power in node.findall('lesser_racial_power'):
        lrp = insert_feature(power).description

    for power in node.findall('moderate_racial_power'):
        mrp = insert_feature(power).description

    for power in node.findall('greater_racial_power'):
        grp = insert_feature(power).description

    features = dict()

    for feature in node.findall('feature'):
        feature_to_insert = insert_feature(feature)
        features[feature_to_insert.name] = feature_to_insert.description
    # subraces
    subraces = None
    for subrace_node in node.findall('subraces')[0].findall('subrace'):
        if subraces is None:
            subraces = list()
        subrace_name = subrace_node.attrib['name'].title()
        subrace_attrib = None
        try:
            subrace_attrib = subrace_node.attrib['attributes']
        except KeyError:
            pass

        subrace_features = dict()
        for subrace_feature_node in subrace_node.findall('feature'):
            subrace_features[subrace_feature_node.attrib['name']] = insert_feature(subrace_feature_node).description

        slrp = None
        smrp = None
        sgrp = None

        for power in subrace_node.findall('lesser_racial_power'):
            slrp = total_tostring(power)

        for power in subrace_node.findall('moderate_racial_power'):
            smrp = total_tostring(power)

        for power in subrace_node.findall('greater_racial_power'):
            sgrp = total_tostring(power)

        subraces.append(Subrace(subrace_name, subrace_attrib, subrace_features, slrp, smrp, sgrp))

    subraces = sorted(subraces, key=lambda sr: sr.name)

    race = Race(name=name, favored_skills=favored_skills, attributes=attributes,
                racial_type=racial_type, size=size, subraces=subraces, movement=movement,
                features=features, lesser_racial_power=lrp, moderate_racial_power=mrp,
                greater_racial_power=grp)
    return race


def xml2feat(node):
    """

    :param node:
    :return:
    """
    name = node.attrib['name'].title()
    benefit = total_tostring(node.findall('benefit')[0])
    prerequisites = [x.text for x in node.findall('prerequisite')]
    if prerequisites == []:
        prerequisites = ['None']
    classes = [x.text for x in node.findall('class')]
    archetypes = [x.text for x in node.findall('archetype')]
    races = [x.text for x in node.findall('race')]
    shortlist = node.findall('short')
    if len(shortlist) != 1:
        raise Exception(
            'Only one shortdescription you little bastard! Better check again ' + name)
    else:
        short = total_tostring(shortlist[0]).strip()
    descriptors = [x.text for x in node.findall('descriptor')]
    if descriptors == []:
        descriptors = ['General']
    specials = [x.text for x in node.findall('special')]
    flavor = node.findall('flavor')[0].text
    if len(specials) == 0:
        special = []
    elif len(specials) == 1:
        special = specials[0]
    else:
        raise Exception('only one special')

    feat = Feat(name=name, benefit=benefit, classes=classes,
                archetypes=archetypes, races=races,
                prerequisites=prerequisites, descriptors=descriptors,
                specials=special, flavor=flavor, short=short)
    return feat


def xml2container(el, container_type=Container):
    """
    loads container from xml
    :param el:
    :param container_type:
    :return:
    """

    kwargs = dict()
    kwargs['name'] = el.attrib['name'].title()
    args, varargs, keywords, defaults = inspect.getargspec(
        container_type.__init__)

    wanted_keys = args
    wanted_keys.remove('self')
    for key in wanted_keys:
        content = get_content(el, key)
        if content is not None:
            kwargs[key] = content
        elif key in el.keys():  # look in attribures
            kwargs[key] = el.attrib[key]

    return container_type(**kwargs)


def xml2spell(el):
    """
    Returns Spell from appropriate xml element

    :type el: Element
    :rtype: Incantation

    >>> x = Spell(name='goo cantation', level=9, description='this way goo', flavor='you are goo', target='some poor guy', skill_checks='no skilla')
    >>> xml = x.get_xml()
    >>> et.tostring(xml)
    b'<Spell name="goo cantation"><level>9</level><description>this way goo</description><flavor>you are goo</flavor><skill_checks>no skilla</skill_checks><target>some poor guy</target></Spell>'

    >>> y = xml2spell(xml)
    >>> type(y)
    <class '__main__.Spell'>
    >>> yml = y.get_xml()
    >>> et.tostring(yml)
    b'<Spell name="goo cantation"><level>9</level><description>this way goo</description><flavor>you are goo</flavor><skill_checks>no skilla</skill_checks><target>some poor guy</target></Spell>'

    """

    return xml2container(el, container_type=Spell)


def xml2utterance(el):
    return xml2container(el, container_type=Utterance)


def xml2item(el):
    return xml2container(el, container_type=Item)


def xml2psipower(el):
    return xml2container(el, container_type=Psipower)


def xml2incantation(el):
    return xml2container(el, container_type=Incantation)


def xml2invocation(el):
    return xml2container(el, container_type=Invocation)


def xml2character(el):
    return xml2container(el, container_type=Character)


def load_things(path, conversion_function):
    tree = et.parse(path)
    root = tree.getroot()
    unsorted = [conversion_function(el) for el in root]
    thing_list = sorted(unsorted, key=lambda x: x.name)
    thing_dict = dict([(remove_spaces(c.name), c) for c in thing_list])

    return thing_list, thing_dict


def load_subraces(racedict):
    realsubracelist = []
    for r in racedict:
        realsubracelist.extend(racedict[r].subraces)
    subracelist = sorted(realsubracelist, key=lambda x: x.name)
    subracedict = dict([(sub.ref, sub) for sub in subracelist])
    return subracelist, subracedict


def load_characters(path=os.path.join('data', 'halloffame.xml')):
    return load_things(path, xml2character)


def load_races(path=os.path.join('data', 'races.xml')):
    racelist, racedict = load_things(path, xml2race)
    subracelist, subracedict = load_subraces(racedict)
    unsorted_completeracelist = racelist + subracelist
    completeracelist = sorted(unsorted_completeracelist, key=lambda x: x.name)
    completeracedict = dict([(sub.ref, sub) for sub in completeracelist])
    return racelist, racedict, subracelist, completeracedict

def load_features(path=os.path.join('data', 'features.xml')):
    return load_things(path, lambda el: xml2container(el=el, container_type=Feature))

def load_archetypes(path=os.path.join('data', 'archetypes.xml')):
    return load_things(path, xml2archetype)


def load_maneuvers(path=os.path.join('data', 'maneuvers.xml')):
    return load_things(path, xml2maneuver)


def load_items(path=os.path.join('data', 'items.xml')):
    return load_things(path, xml2item)


def load_houserules(path=os.path.join('data', 'houserules.xml')):
    return load_things(path, lambda el: xml2container(el=el, container_type=Houserules))


def load_incantations(path=os.path.join('data', 'incantations.xml')):
    return load_things(path, xml2incantation)


def load_spells(path=os.path.join('data', 'spells.xml')):
    return load_things(path, xml2spell)


def load_utterances(path=os.path.join('data', 'utterances.xml')):
    return load_things(path, xml2utterance)


def load_psipowers(path=os.path.join('data', 'psipowers.xml')):
    return load_things(path, xml2psipower)


def load_invocations(path=os.path.join('data', 'invocations.xml')):
    return load_things(path, xml2invocation)


def load_feats(path=os.path.join('data', 'feats.xml')):
    return load_things(path, xml2feat)


def load_classes(path=os.path.join('data', 'classes.xml')):
    return load_things(path, xml2class)


def get_content(el, name, default=None):
    """
    gets the text of el.findall(name)
    :param el:
    :param name:
    :param default:
    :return:
    """
    content_list = el.findall(name)
    if len(content_list) == 0:
        return default
    elif len(content_list) == 1:
        return total_tostring(content_list[0])
    else:
        return [total_tostring(x) for x in content_list]


def remove_spaces(string):
    """
    >>> remove_spaces("peter") == "peter"
    True
    >>> remove_spaces("p e t e r") == "peter"
    True
    """
    return "".join(string.split(" "))


def create_feats_of_thing(things, thingdict, featdict):
    """
    connect feats that are designated to a specific class/archetype/race so that
    class to be able to make it appear on the same html-page as the
    class/archetype/race.
    """

    feats_of_thing = {thg: [] for thg in thingdict}
    # for the races, we still need to add the subraces to enable specific feats also
    # for subraces
    for fid, feat in featdict.items():
        for thg in getattr(feat, things):
            # if the "thing" mentioned in the feat exists (with correct spelling) the feat is
            # added to the list of feats accociated to the "thing".
            if (thg in feats_of_thing):
                feats_of_thing[thg].append((fid, feat))
            # if it doesn't exist you will receive an error message and a list of all
            # existing "things"
            else:
                if things == "classes":
                    thing = things[:-2]  # singular, if plural is built with es
                    #e.g. classES
                else:
                    # singular, if plural is built simply with s
                    thing = things[:-1]
                    #e.g. raceS

                keylist = "\n".join(sorted(list(thingdict.keys())))
                featname = feat.name
                msg = """
In feat {featname} {thing} {thg} appears, but I only know the following {things}:
{keylist}
This might be due to wrong spelling. If your sure that the spelling is
correct, please comment out the line \'<{thing}>{thg}</{thing}>\' at {featname}
in feats.xml which associates the feat to this {thing}
                """.format(**locals())
                raise Exception(msg)

    return feats_of_thing


if __name__ == '__main__':
    import doctest

    doctest.testmod()
