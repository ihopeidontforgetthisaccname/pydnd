from app import app, load_data_from_xml

if __name__ == '__main__':
    load_data_from_xml()
    app.run(host='0.0.0.0', port=8000, debug=True)
