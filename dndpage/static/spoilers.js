var acc = document.getElementsByClassName("accordion");
var i;

var toggler = document.getElementsByClassName("toggle_all").item(0);
if (toggler) {
  toggler.addEventListener("click", function(){
    for (i = 0; i < acc.length; i++) {
      if (acc[i].classList.contains("toggle_all")) {continue;}
      var panel = acc[i].nextElementSibling;
      if (toggler.classList.contains("active")) {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
    }
    toggler.classList.toggle("active");
  });
}

for (i = 0; i < acc.length; i++) {
  if (acc[i].classList.contains("toggle_all")) {continue;}
  acc[i].addEventListener("click", function() {
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
