import collections


def sort_dict_of_lists(dict_of_lists):
    """
    sorts a dict of lists by key and item.name
    return: a list of lists which is sorted
    """
    dict_sorted = collections.OrderedDict(sorted(dict_of_lists.items()))
    for key in dict_sorted:
        dict_sorted[key] = sorted(dict_sorted[key], key=lambda x: x.name)

    return dict_sorted

def is_list_of_strings(checked):
    return bool(checked) and isinstance(checked, list) and all(isinstance(elem, str) for elem in checked)

def dict_ordered_by_attribute(items, attribute):
    """
    items: should have dict properties and should be ordered by name
    attribute: can be a containers or single keys
    returns: A dictionary which contains the attributes as keys and all items with those attributes as values
    >>>
    """
    if not len(items):
        return dict()

    def add_key(tags, item):

        if is_list_of_strings(tags):
            # give us tags as nicely formated keys for our dictionary
            nice_tags = [tag.title() for tag in tags]
            for nice_tag in nice_tags:
                if nice_tag in container:
                    container[nice_tag].append(item)
                else:
                    container[nice_tag] = [item]

        else:
            nice_tag = tags.title()
            if nice_tag in container:
                container[nice_tag].append(item)
            else:
                container[nice_tag] = [item]

    container = dict()

    for key in items:
        item = items[key]
        # gets the values of the class attrubte of the item
        try:
            tags = getattr(item, attribute)
        except:
            print("No Attributes available:", sys.exc_info()[0])
            raise

        add_key(tags, item)


    return container


def listify(listornot):
    """
    >>> listify(2)
    [2]
    >>> listify([2])
    [2]
    """
    if isinstance(listornot, list):
        return listornot
    return [listornot]


if __name__ == '__main__':
    import doctest

    doctest.testmod()
