# -*- coding: utf-8 -*-
import settings
from datacontainers import create_feats_of_thing
from datacontainers import load_archetypes
from datacontainers import load_characters
from datacontainers import load_classes
from datacontainers import load_feats
from datacontainers import load_houserules
from datacontainers import load_incantations
from datacontainers import load_invocations
from datacontainers import load_items
from datacontainers import load_maneuvers
from datacontainers import load_psipowers
from datacontainers import load_races
from datacontainers import load_spells
from datacontainers import load_utterances
from datacontainers import load_features
from flask import Flask, redirect
from flask import render_template
from flask import request
from utils import dict_ordered_by_attribute
from utils import sort_dict_of_lists
from werkzeug.security import generate_password_hash, check_password_hash
import subprocess


app = Flask(__name__)


def load_data_from_xml():
    settings.init_global_features()
    settings.features = load_features()[1]

    global itemlist, itemdict, categoricalitemdict, featlist, featdict, \
        racelist, racedict, subracelist, completeracedict, feats_of_race, \
        classlist, classdict, feats_of_class, archetypes, archetypedict, \
        feats_of_archetype, houserulelist, maneuverlist, maneuverdict, \
        categoricalfeatdict, categoricalmaneuverdict, incantationlist, \
        incantationdict, spelllist, spelldict, categoricalspelldict, \
        utterancelist, utterancedict, categoricalutterancedict, \
        psipowerlist, psipowerdict, invocationlist, invocationdict, \
        hoflist, hofdict
    itemlist, itemdict = load_items()
    categoricalitemdict = sort_dict_of_lists(
        dict_ordered_by_attribute(itemdict, 'slot'))

    featlist, featdict = load_feats()

    racelist, racedict, subracelist, completeracedict = load_races()
    feats_of_race = create_feats_of_thing('races', completeracedict, featdict)

    classlist, classdict = load_classes()
    feats_of_class = create_feats_of_thing('classes', classdict, featdict)

    archetypes, archetypedict = load_archetypes()
    # connect archetypes to classes, cause they are printed on the same html page
    for archetype in archetypes:
        archetype.connect_to_classes(classdict)
    feats_of_archetype = create_feats_of_thing(
        'archetypes', archetypedict, featdict)

    houserulelist, _ = load_houserules()
    maneuverlist, maneuverdict = load_maneuvers()

    categoricalfeatdict = sort_dict_of_lists(
        dict_ordered_by_attribute(featdict, 'descriptors'))

    categoricalmaneuverdict = sort_dict_of_lists(
        dict_ordered_by_attribute(maneuverdict, 'school'))

    incantationlist, incantationdict = load_incantations()

    spelllist, spelldict = load_spells()

    categoricalspelldict = sort_dict_of_lists(
        dict_ordered_by_attribute(spelldict, 'school'))

    utterancelist, utterancedict = load_utterances()

    categoricalutterancedict = sort_dict_of_lists(
        dict_ordered_by_attribute(utterancedict, 'school'))

    psipowerlist, psipowerdict = load_psipowers()

    invocationlist, invocationdict = load_invocations()

    hoflist, hofdict = load_characters()


@app.route("/")
def index():
    return render_template('home.html')


@app.route("/home")
def home():
    return index()


@app.route("/fetch-data", methods=["GET", "POST"])
def fetch_data():
    if request.method == "POST":
        pwd = request.form.get('password', default="")
        h = 'pbkdf2:sha256:260000$bNhFdJ1ZyMHFMZiG$493e3123803cd15ee1b6cd3421e8c061c5f552f5fbe4e8d78b36580aed9282ab'
        if check_password_hash(h, pwd):
            command = "git pull"
            try:
                process = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, encoding="utf-8", timeout=2)
            except subprocess.TimeoutExpired:
                return render_template("fetch-data.html", error=True)
            if process.returncode != 0:
                return render_template("fetch-data.html", error=True)
            load_data_from_xml()
            return redirect("/home")
    return render_template('fetch-data.html', error=False)


@app.route("/character")
def character():
    name = request.args.get('name')
    race_name = request.args.get('race')
    class_name = request.args.get('class')

    subrace_name = request.args.get('subrace')
    subrace = None
    for s in racedict[race_name].subraces:
        if s.name == subrace_name:
            subrace = s
            print("found sth")
            break

    archetype_name = request.args.get('archetype')
    archetype = None
    for a in classdict[class_name].archetypes:
        if a.name == archetype_name:
            archetype = a
            break

    return render_template('characterView.html', name=name, character_class=classdict[class_name], subrace=subrace, archetype=archetype, feats_of_class=feats_of_class, feats_of_archetype=feats_of_archetype, race=racedict[race_name], feats_of_race=feats_of_race)


route_to_template = {
    "halloffame":   lambda ref: render_template('halloffameOverview.html', characters=hoflist),
    "halloffamecharacter":    lambda ref: render_template('halloffame.html', character=hofdict[ref]),
    "items":        lambda ref: render_template('itemOverview.html', items=itemlist, itemcategories=categoricalitemdict),
    "item":         lambda ref: render_template('item.html', item=itemdict[ref]),
    "psipowers":    lambda ref: render_template('psipowerOverview.html', psipowers=psipowerlist),
    "psipower":     lambda ref: render_template('psipower.html', psipower=psipowerdict[ref]),
    "invocations":  lambda ref: render_template('invocationOverview.html', invocations=invocationlist),
    "invocation":   lambda ref: render_template('invocation.html', invocation=invocationdict[ref]),
    "spells":       lambda ref: render_template('spellOverview.html', spells=spelllist, spellcategories=categoricalspelldict),
    "spell":        lambda ref: render_template('spell.html', spell=spelldict[ref]),
    "utterances":   lambda ref: render_template('utteranceOverview.html', utterances=utterancelist, utterancecategories=categoricalutterancedict),
    "utterance":    lambda ref: render_template('utterance.html', utterance=utterancedict[ref]),
    "incantations": lambda ref: render_template('incantationOverview.html', incantations=incantationlist),
    "incantation":  lambda ref: render_template('incantation.html', incantation=incantationdict[ref]),
    "houserules":   lambda ref: render_template('houserulesOverview.html', houserules=houserulelist),
    "classes":      lambda ref: render_template('classOverview.html', classes=classlist),
    "class":        lambda ref: render_template('class.html', c=classdict[ref], feats_of_class=feats_of_class, feats_of_archetype=feats_of_archetype),
    "races":        lambda ref: render_template('raceOverview.html', races=racelist),
    "race":         lambda ref: render_template('race.html', race=racedict[ref], feats_of_race=feats_of_race),
    "feats":        lambda ref: render_template('featOverview.html', feats=featlist, featcategories=categoricalfeatdict),
    "feat":         lambda ref: render_template('feat.html', feat=featdict[ref]),
    "maneuvers":    lambda ref: render_template('maneuverOverview.html', maneuvers=maneuverlist, maneuvercategories=categoricalmaneuverdict),
    "maneuver":     lambda ref: render_template('maneuver.html', maneuver=maneuverdict[ref]),
    "feature":      lambda ref: render_template('feature.html', feature=settings.features[ref])
}


# route for all simple uri's, e.g., those which do not rely on more than one query parameter
@app.route('/<string:path>')
def general_route(path):
    ref = request.args.get('name')
    template_renderer = route_to_template.get(path, None)
    if template_renderer is not None:
        return template_renderer(ref)
    return "<html><h1>404</h1></html>"
