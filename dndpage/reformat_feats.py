"""
Convert feats from word to xml
"""

import os
import re
import xml.etree.ElementTree as et

from run import Feat

path = os.path.join('data', 'blub.xml')
path = 'pener.txt'

with open(path, 'r') as file:

    s = file.read()

feats = s.split('\n\n')


def clean_string(s):

    s = re.sub('\n', '', s)
    s = re.sub(']', '', s)
    return s


def make_feat(s):

    try:
        first, sec = s.split('Benefit:')
    except:
        print(s)
    head, prerequisites = first.split('Prerequisites:')

    prerequisites = prerequisites.split(', ')
    title, flavor, _ = head.split('\n')
    title = title.split('[')
    name = title[0]
    title.pop(0)
    descriptors = [clean_string(desc) for desc in title]

    if 'Special' in sec:
        benefit, special = sec.split('Special:')
    else:
        benefit = sec
        special = []

    print(0, name)
    print(0.1, descriptors)
    print(1, flavor)
    print(2, prerequisites)
    print(3, benefit)
    print(4, special)

    return Feat(specials=special, prerequisites=prerequisites, name=name,
                flavor=flavor, descriptors=descriptors, benefit=benefit)


feats = [make_feat(feat) for feat in feats]
feats = [feat.get_xml() for feat in feats]

root = et.Element('data')
for feat in feats:
    root.append(feat)

tree = et.ElementTree(root)
tree.write('feats.xml')

#
#
# f = feats[0]
# print(f)
# print(make_feat(f))
#
#
#
#
# feat = make_feat(f)
#
# tree = et.ElementTree(feat.get_element())
# tree.write('feats.xml')
